# Static Site Generator with CI/CD

This project demonstrates the usage of static site generators in combination with Continuous Integration (CI) pipelines.

## How to Use
1. Clone this repository
2. Modify or add your Markdown files in the `docs/` directory
3. Push your changes to GitLab

## Installation
1. Install MkDocs (or any other generator)
